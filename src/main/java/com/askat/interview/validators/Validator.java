package com.askat.interview.validators;

import com.askat.interview.dto.Parameter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * This is an implementation for parameter validation.
 * @see Parameter
 */
@Component
public class Validator {
    public void validate(Parameter parameter) {
        Assert.isTrue(2 <= parameter.getX(), "'x' should be greater or equal to 2.");
        Assert.isTrue(7 <= parameter.getY(), "'y' should be greater or equal to 7.");
        Assert.isTrue(5 <= parameter.getZ(), "'z' should be greater or equal to 5.");
    }
}
