package com.askat.interview.services;

import com.askat.interview.dto.SequenceDetails;

/**
 * The <code>PalindromeService</code> interface should be implemented by any
 * class whose instances are intended to process a sequence to produce detail information.
 */
public interface PalindromeService {
    /**
     * When an object implementing interface <code>PalindromeService</code> is used
     * to produce detail information of sequence.
     *
     * @param sequence {@see String}
     * @return {@see SequenceDetails}
     * @see SequenceDetails
     */
    SequenceDetails getDetails(String sequence);
}
