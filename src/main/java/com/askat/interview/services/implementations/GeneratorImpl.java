package com.askat.interview.services.implementations;

import com.askat.interview.dto.Parameter;
import com.askat.interview.dto.SequenceDetails;
import com.askat.interview.dto.SequenceResponse;
import com.askat.interview.properties.SimpleProperty;
import com.askat.interview.services.Generator;
import com.askat.interview.services.PalindromeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.askat.interview.helpers.RandomHelpers.getRandomNumberInRange;
import static com.askat.interview.helpers.StringHelper.getSubstringByThreshold;
import static com.askat.interview.helpers.StringHelper.padStringByThreshold;

/**
 * This class includes methods for generating unique sequence of numbers.
 */
@Service
@Slf4j
public class GeneratorImpl implements Generator {

    private final PalindromeService palindromeService;
    private final SimpleProperty property;

    @Autowired
    public GeneratorImpl(PalindromeService palindromeService, SimpleProperty property) {
        log.info(property.toString());
        this.palindromeService = palindromeService;
        this.property = property;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SequenceResponse generateSequenceDetails(Parameter parameter) {
        return generateSequenceDetails(parameter,
                getRandomNumberInRange(property.getRandomMin(), property.getRandomMax()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SequenceResponse generateSequenceDetails(Parameter parameter, int seed) {

        long startTime = System.nanoTime();

        String sequence = generateSequence(seed, parameter);

        long generateTime = System.nanoTime() - startTime;

        sequence = getSubstringByThreshold(sequence, property.getSubstringThreshold());

        SequenceDetails sequenceDetails = palindromeService.getDetails(sequence);

        return SequenceResponse
                .builder()
                .sequenceDetails(sequenceDetails)
                .sequence(padStringByThreshold(sequence, property.getPaddingThreshold()))
                .generateTime(generateTime)
                .build();
    }

    /**
     * This method generates unique sequence of numbers according to the given task.
     *
     * @param seed      random seed starting point for generating sequence.
     * @param parameter {@code Parameter}
     * @return a sequence {@code String}
     * @see Parameter
     */
    private String generateSequence(int seed, Parameter parameter) {
        StringBuilder sequenceBuilder = new StringBuilder();
        sequenceBuilder.append(seed);

        int i = 0;
        while (i < parameter.getZ()) {
            seed = getNextElement(seed, parameter.getX(), parameter.getY());
            sequenceBuilder.append(seed);
            i++;
        }
        return sequenceBuilder.toString();
    }

    /**
     * If the value of seed is even number, it is divided by x, otherwise the value of y is added.
     * @param seed random seed number
     * @param x a {@code int}
     * @param y a {@code int}
     * @return a {@code int}
     */
    private int getNextElement(int seed, Integer x, Integer y) {
        if (seed % 2 == 0) {
            return seed / x;
        }
        return seed + y;
    }
}
