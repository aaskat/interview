package com.askat.interview.services.implementations;

import com.askat.interview.dto.Palindrome;
import com.askat.interview.dto.SequenceDetails;
import com.askat.interview.dto.SimpleResult;
import com.askat.interview.services.PalindromeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * This service contains method for detecting palindromes as well as returning detail information of it.
 */
@Service
@Slf4j
public class PalindromeServiceImpl implements PalindromeService {
    /**
     * {@inheritDoc}
     */
    @Override
    public SequenceDetails getDetails(String sequence) {
        if (sequence == null) {
            throw new IllegalArgumentException("Sequence is empty.");
        }

        long startTime = System.nanoTime();

        SimpleResult<Integer, List<Palindrome>> result = searchForPalindromes(sequence);

        long endTime = System.nanoTime();
        long searchTime = endTime - startTime;

        List<Palindrome> palindromes = result.getSecond();

        log.info(String.format("All found palindromes: %s", palindromes));
        Palindrome maxPalindrome = palindromes
                .stream()
                .max(Comparator.comparing(Palindrome::length))
                .orElse(null);

        SequenceDetails details = SequenceDetails
                .builder()
                .longestPalindrome(maxPalindrome)
                .numberOfIterations(result.getFirst())
                .numberOfPalindromes(palindromes.size())
                .searchTime(searchTime)
                .build();
        return details;
    }

    /**
     * Find all palindromes within a sequence.
     *
     * @param sequence a {@code String}
     * @return {@code SimpleResult}
     */
    private SimpleResult<Integer, List<Palindrome>> searchForPalindromes(String sequence) {
        List<Palindrome> palindromes = new ArrayList<>();

        int loopCount = 0;
        for (int i = 0; i < sequence.length(); i++) {
            loopCount++;
            // searching for even length palindromes:
            SimpleResult<Integer, List<Palindrome>> entry = searchForPalindromes(sequence, i, i + 1);
            loopCount += entry.getFirst();
            palindromes.addAll(entry.getSecond());

            // searching for odd length palindromes:
            entry = searchForPalindromes(sequence, i, i);
            loopCount += entry.getFirst();
            palindromes.addAll(entry.getSecond());
        }

        return new SimpleResult<>(loopCount, palindromes);
    }

    /**
     * This method looks for a palindrome in a given sequence. To find a palindrome,
     * it performs conditional checks against starting and ending indices. It returns  SimpleResult
     * that contains number of iterations and detected palindrome.
     *
     * @param input an sequence
     * @param start an starting index
     * @param end   an ending index
     * @return {@code SimpleResult}
     */
    private SimpleResult<Integer, List<Palindrome>> searchForPalindromes(String input, int start, int end) {
        List<Palindrome> palindromes = new ArrayList<>();

        int loopCount = 0;
        while (start >= 0 && end < input.length() && input.charAt(start) == input.charAt(end)) {
            String substring = input.substring(start, end + 1);

            if (1 < substring.length())
                palindromes.add(
                        Palindrome
                                .builder()
                                .startIndex(start)
                                .endIndex(end)
                                .subsequence(substring)
                                .build());
            start--;
            end++;
            loopCount++;
        }
        return new SimpleResult<>(loopCount, palindromes);
    }
}
