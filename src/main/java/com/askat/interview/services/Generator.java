package com.askat.interview.services;

import com.askat.interview.dto.Parameter;
import com.askat.interview.dto.SequenceResponse;

/**
 * The <code>Generator</code> interface should be implemented by any
 * class whose instances are intended to generate the unique sequence.
 */
public interface Generator {
    /**
     * When an object implementing interface <code>Generator</code> is used
     * to generate unique sequence by given parameters.
     *
     * @param parameter {@see Parameter}
     * @return {@see SequenceResponse}
     * @see Parameter
     */
    SequenceResponse generateSequenceDetails(Parameter parameter);

    /**
     * When an object implementing interface <code>Generator</code> is used
     * to generate unique sequence by given parameters and seed.
     * @see Parameter
     *
     * @param parameter {@see Parameter}
     * @return {@see SequenceResponse}
     */
    SequenceResponse generateSequenceDetails(Parameter parameter, int seed);
}
