package com.askat.interview.controllers;

import com.askat.interview.dto.SequenceDetails;
import com.askat.interview.services.PalindromeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is an additional class to the task(not required) which allows to calculate any given sequence.
 */
@RestController
@Slf4j
public class PalindromeController {

    private final PalindromeService palindromeService;

    @Autowired
    public PalindromeController(PalindromeService palindromeService) {

        this.palindromeService = palindromeService;
    }

    /**
     * This method is created to initiate another method for calculating palindrome details such as:
     *
     * @param sequence
     * @return
     * @see SequenceDetails
     */

    @GetMapping("/api/get-longest-longestPalindrome")
    public SequenceDetails getRandomStringSequence(@RequestParam() String sequence) {
        log.info(String.valueOf(sequence));
        return palindromeService.getDetails(sequence);
    }
}
