package com.askat.interview.controllers;

import com.askat.interview.dto.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * The following class allows exception handling and it is applied across the whole application,
 * not just to an individual controller. This is the suggested technique by the Spring Documentation.
 */
@ControllerAdvice
@Slf4j
public class ExceptionHandlerAdvice {
    /**
     * The below method handles an exception and generates error response which is communicated back to the client.
     * @param ex a handled Exception object
     * @return an {@code ErrorResponse}
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
        logError(ex);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Logs an exception at the controller level.
     *
     * @param e the exception object to be logged
     */
    private void logError(Exception e) {
        log.error("-------------- ERROR --------------");
        log.error(" MESSAGE: " + e.getMessage());
        log.error("Stacktrace: ", e);
        log.error("-----------------------------------");
    }
}
