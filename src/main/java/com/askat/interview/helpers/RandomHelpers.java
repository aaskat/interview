package com.askat.interview.helpers;

import java.util.Random;

/**
 * This class contains helper methods for random number generation.
 */
public class RandomHelpers {
    static final String BAD_MIN = "min must be positive integer";
    static final String BAD_MAX = "max must be greater than min";

    /**
     * This method generates random value for given range.
     *
     * @param min the least value, unless greater than max. Must be positive integer.
     * @param max the upper bound (inclusive), must not equal or less then min.
     * @return
     */
    public static int getRandomNumberInRange(int min, int max) {
        if (min <= 0)
            throw new IllegalArgumentException(BAD_MIN);

        if (min >= max)
            throw new IllegalArgumentException(BAD_MAX);

        return new Random().nextInt((max - min) + 1) + min;
    }
}
