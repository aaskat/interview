package com.askat.interview.helpers;

/**
 * This class contains helper methods for string processing.
 */
public class StringHelper {
    /**
     * This method returns given string itself if the string length is less or equal to
     * threshold, if the length of the string is greater than threshold it provides substring that matches to the threshold.
     * @param str {@code String}
     * @param threshold {@code int}
     * @return a {@code String}
     */
    public static String getSubstringByThreshold(String str, int threshold) {
        return str.length() <= threshold
                ? str
                : str.substring(0, threshold);
    }

    /**
     * If the length of the string is less than threshold, it supplements characters to match the threshold.
     * @param str {@code String}
     * @param threshold {@code int}
     * @return a {@code String}
     */
    public static String padStringByThreshold(String str, int threshold) {
        return str.length() < threshold
                ? String.format("%1$-" + threshold + "s", str).replace(' ', '0')
                : str;
    }
}
