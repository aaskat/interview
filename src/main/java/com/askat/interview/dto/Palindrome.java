package com.askat.interview.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * This class represents information about palindrome.
 *
 */
@Data
@Builder
public class Palindrome {
    /**
     * This indicates the starting index of a palindrome.
     */
    private int startIndex;

    /**
     * This indicates the ending index of a palindrome.
     */
    private int endIndex;

    /**
     * The palindrome subsequence.
     */
    private String subsequence;

    /**
     * @return the length of the sequence of characters represented by this object
     */
    @JsonProperty("length")
    public int length() {
        return subsequence.length();
    }
}
