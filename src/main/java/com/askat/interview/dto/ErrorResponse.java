package com.askat.interview.dto;

import lombok.Data;

/**
 * The class can be used as the return value from a ExceptionHandlerAdvice.
 * @see com.askat.interview.controllers.ExceptionHandlerAdvice
 * The class constructed using Lombok @Data annotation to improve readability of the code.
 * @see <a href="https://projectlombok.org">Lombok</a>
 */

@Data
public class ErrorResponse {
    /**
     * The HTTP status codes are:
     * @see <a href="https://en.wikipedia.org/wiki/List_of_HTTP_status_codes">HTTP Status Codes</a>
     */
    private int httpStatus;

    /**
     * This is error message taken from Exception Object.
     */
    private String message;

    public ErrorResponse(int httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }
}