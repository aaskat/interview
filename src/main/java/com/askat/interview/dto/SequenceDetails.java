package com.askat.interview.dto;

import lombok.Builder;
import lombok.Data;

/**
 * This class represents the detail information of the sequence.
 */
@Data
@Builder
public class SequenceDetails {
    /**
     * @see Palindrome
     */
    private Palindrome longestPalindrome;

    /**
     * Number of palindromes found within a sequence.
     */
    private int numberOfPalindromes;

    /**
     * Number of iterations to find all palindromes within a sequence.
     */
    private int numberOfIterations;

    /**
     * Elapsed time to find all palindromes(ms).
     */
    private long searchTime;
}
