package com.askat.interview.dto;

import lombok.Builder;
import lombok.Data;

/**
 * This class represents detail of sequence.
 * For instantiation use Lombok @Builder annotation.
 * @see <a href="https://projectlombok.org/features/Builder">Lombok @Builder</a>
 */

@Data
@Builder
public class SequenceResponse {
    /**
     * This value represents given sequence in string.
     */
    private String sequence;

    /**
     * @see SequenceDetails
     */
    private SequenceDetails sequenceDetails;

    /**
     * Elapsed time to generate sequence(ms).
     */
    private long generateTime;
}
