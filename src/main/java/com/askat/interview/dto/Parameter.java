package com.askat.interview.dto;

import lombok.Data;

/**
 * This class represents parameter requirements for generating random sequences.
 */
@Data
public class Parameter {
    /**
     * This is an optional value divides given random number, if the number is even. According to the task definitions the default value is 2.
     */
    private Integer x = 2;
    /**
     * This is an optional value added to the random number, if the number is odd. The default value is 7.
     */
    private Integer y = 7;
    /**
     * This is an optional value for iteration to produce a sequence. The default value is 5.
     */
    private Integer z = 5;
}