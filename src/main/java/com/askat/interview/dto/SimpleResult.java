package com.askat.interview.dto;


import lombok.Getter;

/**
 * A container object which used to return two values from method
 * to achieve Python like syntax
 * def some_method():
 *     first_value = 1
 *     second_value = 2
 *     return (first_value, second_value)
 * @param <T> the type of first value
 * @param <E> the type of second value
 */

public final class SimpleResult<T, E> {

    @Getter
    private final T first;

    @Getter
    private final E second;

    public SimpleResult(T t, E e) {
        first = t;
        this.second = e;
    }
}
