package com.askat.interview.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * This class represents properties for random sequence generation.
 */
@ConfigurationProperties(ignoreUnknownFields = false, prefix = "interview")
@Data
public class SimpleProperty {
    /**
     * interview.padding-threshold=10
     */
    private int paddingThreshold = 30;

    /**
     * interview.substring-threshold=20
     */
    private int substringThreshold = 40;

    /**
     * interview.random-min=10000
     */
    private int randomMin = 10_000;

    /**
     * interview.random-max=100000
     */
    private int randomMax = 100_000;
}