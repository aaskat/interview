package com.askat.interview;

import com.askat.interview.dto.Parameter;
import com.askat.interview.dto.SequenceResponse;
import com.askat.interview.dto.SequenceDetails;
import com.askat.interview.properties.SimpleProperty;
import com.askat.interview.services.Generator;
import com.askat.interview.services.PalindromeService;
import com.askat.interview.services.implementations.GeneratorImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class GeneratorImplTest {
    private Generator generator;
    private Parameter parameter;

    @Mock
    PalindromeService palindromeService;

    @Before
    public void setup() {
        parameter = new Parameter();
        parameter.setX(2);
        parameter.setY(7);
        parameter.setZ(5);

        SimpleProperty property = new SimpleProperty();
        property.setPaddingThreshold(30);
        property.setSubstringThreshold(40);
        property.setRandomMin(10_000);
        property.setRandomMax(100_000);

        generator = new GeneratorImpl(palindromeService, property);
    }

    @Test
    public void getGeneratedSequence_ReturnExpectedSequence() {
        String sequence = "4242422121212121281060645303226516";

        when(palindromeService.getDetails(sequence)).thenReturn(SequenceDetails.builder().build());

        SequenceResponse sequenceResponse = generator.generateSequenceDetails(parameter, 424242);
        log.info(sequenceResponse.toString());

        assertEquals(sequence, sequenceResponse.getSequence());

        verify(palindromeService, times(1)).getDetails(sequence);
    }
}