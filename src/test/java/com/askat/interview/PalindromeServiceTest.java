package com.askat.interview;

import com.askat.interview.dto.SequenceDetails;
import com.askat.interview.services.PalindromeService;
import com.askat.interview.services.implementations.PalindromeServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class PalindromeServiceTest {
    private PalindromeService palindromeService;

    @Before
    public void setup() {
        palindromeService = new PalindromeServiceImpl();
    }

    @Test
    public void getDetails_HasNotPalindrome() {
        String p = "abcdefghijklmnopqrstuvwxyz";
        SequenceDetails details = palindromeService.getDetails(p);
        log.info(details.toString());
        assertNull(details.getLongestPalindrome());
        assertEquals(details.getNumberOfPalindromes(), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDetails_InvalidSequenceThrowsException() {
        palindromeService.getDetails(null);
    }

    @Test
    public void getDetails_HasOnePalindrome() {
        String p = "abcdefghijklabbamnopqrstuvwxyz";
        SequenceDetails details = palindromeService.getDetails(p);
        log.info(details.toString());
        assertEquals(details.getNumberOfPalindromes(), 2);
        assertEquals(details.getLongestPalindrome().getSubsequence(), "abba");
        assertEquals(details.getLongestPalindrome().getStartIndex(), 12);
        assertEquals(details.getLongestPalindrome().getEndIndex(), 15);
    }
}
